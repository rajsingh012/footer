import React, { Fragment } from 'react';
import FooterTop from './views/footerTopSection';
import LogoContainer from './views/logoContainer';
import LinkContainer from './views/linkContainer';
import SocialContainer from './views/socialContainer';
import ImageContainer from './views/imageContainer';
import './index.less';

const FooterComp = (props) => {
    const {
        logoUrl = "",
        emailData = "",
        contact = "",
        comapnyName = "",
        footerData = [],
        year = "",
        isB2C = false,
        productType = "",
        metaData = {},
        footerTop = {},
        rightsReservedText= ''
    } = props;
    const containerClass = isB2C ? 'b2c' : 'b2b';
    const {footerBgColor= '', footerTextColor = ''} = metaData;
    return (
        <footer className={`mainFooter ${containerClass}`} style={{background: footerBgColor}}>
            {!isB2C && <FooterTop
                emailData={emailData}
                contact={contact}
                footerTopData={footerTop}
            />}
            <div className="footer_container">
                <div className='footer__mainWrap'>
                    {!isB2C &&
                        <LogoContainer
                            logoUrl={logoUrl}
                            year={year}
                            comapnyName={comapnyName}
                            rightsReservedText={rightsReservedText}
                        />}
                    <div className="footer__mainWrap--menusection">
                        {footerData.length > 0 && footerData.map((data, key) => {
                            const { type = '', displayLabel = '', options = [], additionalProp = {} } = data;
                            return (
                                <Fragment key={key}>
                                    {type === 'linkContainer' &&
                                        <LinkContainer
                                            options={options}
                                            heading={displayLabel}
                                            footerTextColor = {footerTextColor}
                                        />}
                                    {type === 'socialContainer' &&
                                        <SocialContainer
                                            options={options}
                                            heading={displayLabel}
                                            productType={productType}
                                            footerTextColor= {footerTextColor}
                                        />}
                                    {type === 'imageContainer' &&
                                        <ImageContainer
                                            heading={displayLabel}
                                            additionalProp={additionalProp}
                                            footerTextColor = {footerTextColor}
                                        />
                                    }
                                </Fragment>
                            )
                        })
                        }
                    </div>
                </div>
            </div>
        </footer>
    )
}

export default FooterComp;
