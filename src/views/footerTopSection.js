import React, { Fragment } from 'react';

const FooterTop = (props) => {
    const { emailData = '', contact = '', footerTopData = {} } = props;
    const { heading = 'How can we help you?', subHeading = 'Contact us anytime.', emailUsText = 'SEND US A EMAIL AT', callUsText = 'OR CALL US AT' } = footerTopData;
    return ( 
        <div className="footer__topwrap">
            <div className="footer_container">
                <div className="footer__topwrap--leftside">
                    <h3 className="footer__topwrap--helphead">{heading}</h3>
                    <h5 className="footer__topwrap--contacthead">{subHeading}</h5>
                </div>
                <div className="footer__topwrap--rightside">
                    <div className="footer__topwrap--detailbox">
                        <div className="footer__topwrap--detailleft">
                            <p>{emailUsText}</p>
                            <p>{emailData}</p>
                        </div>
                        <div className="footer__topwrap--detailright">
                            <p>{callUsText}</p>
                            <p>{contact}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default FooterTop
