import React from 'react';

const ImageContainer = (props) => {
    const { heading = '', additionalProp = {} } = props;
    const {src = '', link = '', target = '_self', imgWidth ='', footerTextColor = '' } = additionalProp;
    
    return (
        <div className="footer__mainWrap--menuwrap">
            <h3 className="footer__mainWrap--listhead" style={{color:footerTextColor}}>{heading}</h3>
            {<a href={link} target={target}><img className="footer__mainWrap--img" src={src} width={`${imgWidth}%`}/></a>}
        </div>
    )
}

export default ImageContainer
