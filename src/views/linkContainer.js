import React from 'react';

const LinkContainer = (props) => {
    const { heading = '', options = [], footerTextColor = ''} = props;
    return (
        <div className="footer__mainWrap--menuwrap">
            <h3 className="footer__mainWrap--listhead" style={{color:footerTextColor}}>{heading}</h3>
            <ul className="footer__mainWrap--menuul">
                {options.map((data, key) => {
                    const { displayLabel = '', link = '', target= '_self' } = data;
                    return (
                        <li className="footer__mainWrap--menulist" key={key}><a className="footer__mainWrap--anchor-link" href={link} target={target} style={{color:footerTextColor}}>{displayLabel}</a></li>
                    )
                })
                }
            </ul>
        </div>
    )
}

export default LinkContainer
