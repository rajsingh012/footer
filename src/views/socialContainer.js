import React from 'react';

const SocialContainer = (props) => {
    const { heading = '', options = [], productType = '', footerTextColor = '' } = props;
    return (
        <div className="footer__mainWrap--menuwrap">
            <h3 className="footer__mainWrap--listhead" style={{color:footerTextColor}}>{heading}</h3>
            <div className="footer__mainWrap--socialMenu">
                {options.map((data, key) => {
                    const { icon = {}, link = '', target = '_self', bgColor = '', color = '' } = data;
                    return (
                        <a href={link} target={target} class="footer__mainWrap--socialLink" key={key} style={{ backgroundColor: bgColor, color: color }}><i className={icon[productType]} aria-hidden="true"></i></a>
                    )
                })
                }
            </div>
        </div>
    )
}

export default SocialContainer
