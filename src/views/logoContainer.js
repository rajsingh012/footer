import React from 'react';

const LogoContainer = (props) => {
    const { logoUrl = '', year = '', comapnyName = '', rightsReservedText = 'All Rights Reserved.' } = props;
    return (
        <div className="footer__mainWrap--logosection">
            {logoUrl ? <img className="footer__mainWrap--footerlogo" src={logoUrl} /> : <h3 className="footer__mainWrap--companylogo">Logo Here</h3>}
            <p>&copy; {year} {comapnyName}. {rightsReservedText}</p>
        </div>
    )
}

export default LogoContainer
